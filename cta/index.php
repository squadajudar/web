<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
        <title>Doações - Engenheiros sem fronteiras</title>
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic">
        <link rel="stylesheet" href="assets/fonts/font-awesome.min.css">
        <link rel="shortcut icon" href="http://gabrielhenriq.com.br/efs/wp-content/themes/esfbrasil/assets/img/favicon.png" />
        <style>
            @import url('https://fonts.googleapis.com/css2?family=Lato&family=Roboto:wght@300&display=swap');
        </style>
    </head>
    <?php
    $fundo = "http://gabrielhenriq.com.br/efs/wp-content/themes/esfbrasil/assets/img/tint.png";
    $img = "http://gabrielhenriq.com.br/efs/wp-content/uploads/2020/05/integre-se-3.jpg";
    if (false) {
        $img = "assets/img/home-bg.png";
    }
    ?>
    <body>
        <nav class="navbar navbar-light navbar-expand-lg fixed-top" id="mainNav">
            <div class="container"> 
                <a  href="http://gabrielhenriq.com.br/efs/cta">
                    <img src="assets/img/logo.png" style="height: 120px;">
                </a>
            </div>
        </nav>
        <header class="masthead bg-home" style="background-image:url(<?= $fundo ?>), url(<?= $img ?>);">
            <div class="overlay"></div>
            <div class="container">
                <div class="row">
                    <div class="col-md-10 col-lg-8 mx-auto">
                        <div class="site-heading">
                            <h2>sua doação pode mudar a vida de muitas pessoas</h2>
                            <span class="subheading">
                                Engenheiros sem Fronteiras - BH
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <?php include 'config.php'; ?>  
        <nav class="navbar navbar-dark bg-dark">
            <div style="width: 18%;"></div>
            <span class="navbar-text">
                <h2 class="doacao navbar-brand"><?= $type ?></h2>
            </span>
            <?php if ($type == "PayPal") { ?>
                <h2 class="donate-2"><a class="navbar-brand color-cinza" href="<?= BASE ?>?donate=dep">doação por depósito</a></h2>
            <?php } else { ?>
                <h2 class="donate-2"><a class="navbar-brand color-cinza" href="<?= BASE ?>">doação por PayPal</a></h2>
            <?php } ?>
        </nav>
        <br>
        <div class="container">
            <form name="doacao" id="doacao" action="process" novalidate="novalidate" method="post">
                <div class="control-group">
                    <div class="control-group" >
                        Valor da Doação:
                        <select class="form-control" id="select_val" name="select_val">
                            <option value="10,00">R$ 10,00</option>
                            <option value="15,00">R$ 15,00</option>
                            <option value="20,00">R$ 20,00</option>
                            <option value="-" >Outro Valor</option>
                        </select>
                    </div>
                </div>
                <div class="control-group" id="show-value" style="display: none">
                    <input class="form-control" type="text" name="value" id="value"  placeholder="Valor para Doação"> 
                </div>
                <div class="control-group">
                    <label>Nome:</label> 
                    <input class="form-control" type="text" name="nome" id="nome" required > 
                </div>
                <div class="control-group">
                    <label>Sobrenome:</label> 
                    <input class="form-control" type="text" name="sobrenome" id="sobrenome" required > 
                </div>
                <div class="control-group">
                    <label>E-mail:</label> 
                    <input class="form-control" type="email" name="email" id="email" required > 
                </div>
                <input class="form-control" type="hidden" name="type" id="type" required value="<?= $type ?>"> 
                <br>
                <?php if (SHOW_ADD) { ?>
                    <h2> 2- Endereço </h2>
                    <div class="control-group">
                        <div class="form-group floating-label-form-group controls"> 
                            <label>CEP</label> 
                            <input class="form-control" type="text" name="cep" id="cep" required placeholder="CEP"> 
                        </div>
                    </div>
                    <div class="control-group">
                        <div class="form-group floating-label-form-group controls"> 
                            <label>Logradouro</label> 
                            <input class="form-control" type="text" name="rua" id="rua" required placeholder="Logradouro"> 
                        </div>
                    </div>
                    <div class="control-group">
                        <div class="form-group floating-label-form-group controls"> 
                            <label>Complemento</label> 
                            <input class="form-control" type="text" name="complemento" id="complemento" required placeholder="Complemento"> 
                        </div>
                    </div>
                    <div class="control-group">
                        <div class="form-group floating-label-form-group controls"> 
                            <label>Número</label> 
                            <input class="form-control" type="text" name="numero" id="numero" required placeholder="Número"> 
                        </div>
                    </div>
                    <div class="control-group">
                        <div class="form-group floating-label-form-group controls"> 
                            <label>Bairro</label> 
                            <input class="form-control" type="text" name="bairro" id="bairro" required placeholder="Bairro"> 
                        </div>
                    </div>
                    <div class="control-group">
                        <div class="form-group floating-label-form-group controls"> 
                            <label>Cidade</label> 
                            <input class="form-control" type="text" name="cidade" id="cidade" required placeholder="Cidade"> 
                        </div>
                    </div>
                    <div class="control-group">
                        <div class="form-group floating-label-form-group controls"> 
                            <label>Estado</label> 
                            <input class="form-control" type="text" name="uf" id="uf" required placeholder="Estado"> 
                        </div>
                    </div>
                <?php } ?>
                <div id="success"></div>
                <div class="control-group">
                    <div class="form-group btn-custom"><button class="btn btn-success" id="sendMessageButton" type="submit">prosseguir com a doação</button></div>
                </div>
            </form>
        </div>
        <footer>
        </footer>
        <div class="col-md-12 col-lg-12 ">
            <div class="row">
                <div class="col-md-12 col-lg-12 ">
                    <ul class="list-inline text-center">
                        <li class="list-inline-item facebook"><span class="fa-stack fa-lg"><i class="fa fa-square fa-stack-2x"></i><i class="fa fa-facebook fa-stack-1x fa-inverse"></i></span></li>
                        <li class="list-inline-item insta"><span class="fa-stack fa-lg"><i class="fa fa-square fa-stack-2x"></i><i class="fa fa-instagram fa-stack-1x fa-inverse"></i></span></li>
                        <li class="list-inline-item linkedin"><span class="fa-stack fa-lg"><i class="fa fa-square fa-stack-2x"></i><i class="fa fa-linkedin fa-stack-1x fa-inverse"></i></span></li>
                        <li class="list-inline-item youtube"><span class="fa-stack fa-lg"><i class="fa fa-square fa-stack-2x"></i><i class="fa fa-youtube fa-stack-1x fa-inverse"></i></span></li>
                    </ul>
                    <p class="text-center text-muted copyright">Copyright&nbsp;©&nbsp;Engenheiros sem Fronteiras - BH <?= date('Y') ?></p>
                </div>
            </div>
        </div>
        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="assets/js/clean-blog.js"></script>
        <script src="assets/js/validate.js"></script>
        <script src="assets/js/scrips.js"></script>
        <script src="assets/js/jquery.mask.js"></script>

        <script>
            $("#select_val").change(function () {
                var opt = $(this).val();
                if (opt === '-') {
                    $("#show-value").show();
                    $("#value").attr("required", "req");
                } else {
                    $("#show-value").hide();
                    $("#value").removeAttr('required');
                }
            });
        </script>
    </body>
</html>