<div class="container">
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-lg-8 mx-auto deps-class">
                <div id="paypal-button-container"></div>
                <script>
                    paypal.Buttons({
                        createOrder: function (data, actions) {
                            return actions.order.create({
                                purchase_units: [{
                                        amount: {
                                            value: '<?= $value ?>',
                                            currency: 'BRL'
                                        }
                                    }]
                            });
                        }
                    }).render('#paypal-button-container');
                </script>
            </div>
        </div>
    </div>
</div>