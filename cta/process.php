<?php
if (empty($_POST)) {
    die('Pode passar sem post nn jovem');
}
$data = $_POST;
$name = $data["nome"];
$value = str_replace([".", ","], ["", "."], $data["value"]);
if ($data['select_val'] != "-") {
    $value = str_replace([".", ","], ["", "."], $data["select_val"]);
}

$id = "AVlPiwBhdQKWUOfxOo2Q63Y1DDc2UsypgkHx6Z_fwHE3SaeaKkBH0q9ETjFOxLbtAqr6qLGwSqcvgM7U";

require("source/class.phpmailer.php");

date_default_timezone_set('Brazil/East');
$email = new PHPMailer();
$email->IsHTML(true);
$email->Host = "mail.gabrielhenriq.com.br";
$email->SMTPAuth = true;
$email->SMTPSecure = "ssl";
$email->Port = 465;
$email->SetFrom('naoresponda@gabrielhenriq.com.br', 'CTA');
$email->Username = "naoresponda@gabrielhenriq.com.br";
$email->Password = "dO4!a.b~$!Sy";
$email->AddAddress("gabrielhenriquesp@gmail.com", "CTA");
$email->Subject = utf8_decode("CTA - Engenheiros sem fronteiras BH");
$email->MsgHTML(utf8_decode("Nova Doação:<br /><br />"
                . "Nome: " . $data['nome'] . " " . $data["sobrenome"] . "<br />"
                . "E-mail: " . $data['email'] . "<br />"
));
if (!$email->Send()) {
    var_dump("falha ao enviar o email");
}

//Grava XML
gravaXml($data);

function gravaXml(array $data)
{
    $arquivo_xml = simplexml_load_file('doacoes.xml');
    $xml .= '<?xml version="1.0" encoding="UTF-8"?><usuarios>';

    foreach ($arquivo_xml as $user) {
        $xml .= '<usuario>';
        $xml .= '<nome>' . $user->nome . '</nome>';
        $xml .= '<sobrenome>' . $user->sobrenome . '</sobrenome>';
        $xml .= '<email>' . $user->email . '</email>';
        $xml .= '<tipo>' . $user->tipo . '</tipo>';
        $xml .= '</usuario>';
    }

    $xml .= '<usuario>';
    $xml .= '<nome>' . $data['nome'] . '</nome>';
    $xml .= '<sobrenome>' . $data['sobrenome'] . '</sobrenome>';
    $xml .= '<email>' . $data['email'] . '</email>';
    $xml .= '<tipo>' . $data["type"] . '</tipo>';
    $xml .= '</usuario>';

    $xml .= '</usuarios>';

    $fp = fopen('doacoes.xml', 'w+');
    fwrite($fp, $xml);
    fclose($fp);
}
?>


<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
        <title>Doações - Engenheiros sem fronteiras</title>
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic">
        <link rel="stylesheet" href="assets/fonts/font-awesome.min.css">
    </head>
    <body>
        <script src="https://www.paypal.com/sdk/js?client-id=<?= $id ?>&currency=BRL"></script>
        <nav class="navbar navbar-light navbar-expand-lg fixed-top" id="mainNav">
            <div class="container"> 
                <a  href="http://gabrielhenriq.com.br/efs/cta">
                    <img src="assets/img/logo.png" style="height: 120px;">
                </a>
            </div>
        </nav>
        <header class="masthead bg-home" style="background-image:url('assets/img/home-bg.png');">
            <div class="overlay"></div>
            <div class="container">
                <div class="row">
                    <div class="col-md-10 col-lg-8 mx-auto">
                        <div class="site-heading">
                            <h2>Sua doação pode mudar a vida de muitas pessoas</h2>
                            <span class="subheading">
                                Engenheiros sem fronteiras - BH
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <?php
        if ($data["type"] == "PayPal") {
            include 'paypal.php';
        } else if ($data["type"] == "Depósito") {
            include 'deposito.php';
        }
        ?>
        <footer>
            <div class="container">
                <div class="row">
                    <div class="col-md-10 col-lg-8 mx-auto">
                        <ul class="list-inline text-center">
                            <li class="list-inline-item"><span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-twitter fa-stack-1x fa-inverse"></i></span></li>
                            <li class="list-inline-item"><span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-facebook fa-stack-1x fa-inverse"></i></span></li>
                            <li class="list-inline-item"><span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-github fa-stack-1x fa-inverse"></i></span></li>
                        </ul>
                        <p class="text-muted copyright">Copyright&nbsp;©&nbsp;Brand 2020</p>
                    </div>
                </div>
            </div>
        </footer>
        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="assets/js/clean-blog.js"></script>
    </body>

</html>