<?php get_header('index'); ?>

<div class="page">

    <?php for ( $n = 1; $n <= 5; $n++ ) : ?>
        <?php if ( ! empty(get_theme_mod('index_section' . $n . '_page', '')) ) : ?>
        <div class="section <?php echo get_theme_mod('index_section' . $n . '_color', ''); ?>">
            <div class="container">
                <h2 class="title"><?php echo get_theme_mod('index_section' . $n . '_titulo', ''); ?></h2>
                <?php
                    global $post;
                    $post = get_post( get_theme_mod('index_section' . $n . '_page', '') );
                    setup_postdata( $post );
                    the_content();
                ?>
            </div>
        </div>
        <?php endif; ?>
    <?php endfor; ?>


    <?php if ( get_theme_mod('index_footer_enabled', false) ) : ?>
    <div class="section green">
        <div class="container">
            <div class="row">
                <div class="col-md-6 split">
                    <?php if ( ! empty(get_theme_mod('index_footer_page', '')) ) : ?>
                    <h2 class="title text-left"><?php echo get_theme_mod('index_footer_titulo', ''); ?></h2>
                    <?php
                        global $post;
                        $post = get_post( get_theme_mod('index_footer_page', '') );
                        setup_postdata( $post );
                        the_content();
                    ?>
                    <?php else : ?>
                        &nbsp;
                    <?php endif; ?>
                </div>
                <div class="col-md-6">
                    <div class="text-right" style="position: absolute; bottom: 0; right: 0;">
                        <?php if (!empty(get_theme_mod('redessociais_facebook', ''))) { ?>
                            <a href="<?php echo get_theme_mod('redessociais_facebook', ''); ?>" target="_blank">
                                <i class="fab fa-facebook fa-2x"></i>
                            </a>&nbsp;
                        <?php } ?>

                        <?php if (!empty(get_theme_mod('redessociais_instagram', ''))) { ?>
                            <a href="<?php echo get_theme_mod('redessociais_instagram', ''); ?>" target="_blank">
                                <i class="fab fa-instagram fa-2x"></i>
                            </a>&nbsp;
                        <?php } ?>

                        <?php if (!empty(get_theme_mod('redessociais_linkedin', ''))) { ?>
                            <a href="<?php echo get_theme_mod('redessociais_linkedin', ''); ?>" target="_blank">
                                <i class="fab fa-linkedin fa-2x"></i>
                            </a>
                        <?php } ?>

                        <?php if (!empty(get_theme_mod('redessociais_youtube', ''))) { ?>
                            <a href="<?php echo get_theme_mod('redessociais_youtube', ''); ?>" target="_blank">
                                <i class="fab fa-youtube fa-2x"></i>
                            </a>
                        <?php } ?>

                        <?php
                            $email_contato = get_theme_mod('redessociais_email', '');
                            if (!empty($email_contato) && strstr($email_contato, '@')) {
                        ?>
                            <br>
                            <script type="text/javascript">
                                u = "<?php echo explode('@', $email_contato)[0]; ?>";
                                d = "<?php echo explode('@', $email_contato)[1]; ?>";
                                document.write(u);
                                document.write('&#64;');
                                document.write(d);
                            </script>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php endif; ?>

</div>

<?php get_footer(); ?>
