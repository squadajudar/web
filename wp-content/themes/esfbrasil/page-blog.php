<?php
/*
Template Name: Listagem do Blog (não utilizar para posts)
Template Post Type: post, page
*/
?>

<?php get_header(); ?>


<div class="page">
    <div class="container">
        <div class="page-content">
            <h1 class="title">Blog do ESF</h1>

            <?php
            	$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
				$args = array(
				  'posts_per_page' => 10,
				  'paged'          => $paged
				);

                query_posts( $args );
                if ( have_posts() ) {
                    while ( have_posts() ) {
                        the_post();
            ?>
                        <div class="media">

                          <?php
                            if (get_the_post_thumbnail_url()) {
                                echo '<a href="'; the_permalink(); echo '">';
                                echo '<div width="300px" height="200px">';
                                echo '<img class="mr-3" style="width: 300px; max-height: 200px; object-fit: cover;" src="' . get_the_post_thumbnail_url() . '">';
                                echo '</div>';
                                echo '</a>';
                            }
                          ?>

                          <div class="media-body">
                            <a href="<?php the_permalink(); ?>"><h5 class="mt-0"><?php the_title(); ?></h5></a>
                            <?php the_excerpt(); ?>
                          </div>
                        </div>
            <?php
                    }

					previous_posts_link( 'Postagem mais novas' );
					next_posts_link( 'Postagens mais antigas' );
                } else {
                    echo "Nenhuma postagem ainda";
                }
            ?>
        </div>
    </div>
</div>

<?php get_footer(); ?>
