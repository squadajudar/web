<?php
/*
Template Name: Margens estreitas
*/
?>

<?php get_header(); ?>

<div class="page">
	<div class="container">
		<div class="col-md-10 col-md-offset-1">
	    	<div class="page-content">
		    <?php
			    // Start the Loop.
			    while ( have_posts() ) : the_post();
			    	if (!has_post_thumbnail()) {
			            the_title('<h1 class="title">', '</h1>');
			        }
				    the_content();

				    // If comments are open or we have at least one comment, load up the comment template.
				    if ( comments_open() || get_comments_number() ) :
					    comments_template();
				    endif;
			    endwhile;
		    ?>
		    </div>
		</div>
	</div>
</div>

<?php get_footer(); ?>
