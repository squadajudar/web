<?php
require_once get_template_directory() . '/core/class-bootstrap-nav.php';
require_once get_template_directory() . '/core/class-wp-bootstrap-nav-walker.php';

add_theme_support( 'custom-header', array(
    'default-image' => get_template_directory_uri() . '/assets/img/banner2.png',
));


add_theme_support( 'editor-styles' );
add_editor_style( 'assets/css/editor.css' );

add_theme_support( 'title-tag' );
add_theme_support( 'post-thumbnails' );

function register_my_menus() {
  register_nav_menus(
    array(
      'header-menu' => __( 'Menu cabeçalho' ),
      'footer-menu'  => __( 'Menu rodapé' )
    )
  );
}
add_action( 'init', 'register_my_menus' );

/**
 * Filter the "read more" excerpt string link to the post.
 *
 * @param string $more "Read more" excerpt string.
 * @return string (Maybe) modified "read more" excerpt string.
 */
function wpdocs_excerpt_more( $more ) {
    if ( ! is_single() ) {
        $more = sprintf( '<a class="read-more" href="%1$s">%2$s</a>',
            get_permalink( get_the_ID() ),
            __( ' leia mais...', 'textdomain' )
        );
    }
 
    return $more;
}
add_filter( 'excerpt_more', 'wpdocs_excerpt_more' );

add_filter( 'excerpt_length', function($length) {
    return 120;
} );

function esfbrasil_customize_register($wp_customize){
	$wp_customize->remove_section('colors');

    // Logo
    $wp_customize->add_section('esfbrasil_logo_section', array(
        'title'    => __('Logo', 'esfbrasil'),
        'description' =>  'Configuração da logo do núcleo. Deve ter o fundo transparente e não possuir margem. Tamanho mínimo 300x120.',
        'priority' => 120,
    ));
    $wp_customize->add_setting('logo_img', array(
        'default' => get_template_directory_uri().'/assets/img/logo.png',
    ));
    $wp_customize->add_control( new WP_Customize_Image_Control($wp_customize, 'logo_img_ctl', array(
        'label'    => __('Logotipo ESF', 'esfbrasil'),
        'section'  => 'esfbrasil_logo_section',
        'settings' => 'logo_img',
    )));
    $wp_customize->add_setting('logo_img_white', array(
        'default' => get_template_directory_uri().'/assets/img/logo-white.png',
    ));
    $wp_customize->add_control( new WP_Customize_Image_Control($wp_customize, 'logo_img_white_ctl', array(
        'label'    => __('Logotipo ESF Branco', 'esfbrasil'),
        'section'  => 'esfbrasil_logo_section',
        'settings' => 'logo_img_white',
    )));

	// Banner
    $wp_customize->add_section('esfbrasil_banner_section', array(
        'title'    => __('Banner', 'esfbrasil'),
        'description' => 'Personalização do banner da página inicial',
        'priority' => 121,
    ));
    $wp_customize->add_setting('banner_titulo', array(
        'default'        => 'Título do banner',
    ));
    $wp_customize->add_control('esfbrasil_banner_titulo', array(
        'label'      => __('Título', 'esfbrasil'),
        'section'    => 'esfbrasil_banner_section',
        'settings'   => 'banner_titulo',
    ));
    $wp_customize->add_setting('banner_subtitulo', array(
        'default'        => 'Subtítulo do banner',
    ));
    $wp_customize->add_control('esfbrasil_banner_subtitulo', array(
        'label'      => __('Subtítulo', 'esfbrasil'),
        'section'    => 'esfbrasil_banner_section',
        'settings'   => 'banner_subtitulo',
    ));
    $wp_customize->add_setting('banner_cta_text', array(
        'default'        => 'Botão 1',
    ));
    $wp_customize->add_control('esfbrasil_banner_cta_text', array(
        'label'      => __('Texto do botão 1', 'esfbrasil'),
        'section'    => 'esfbrasil_banner_section',
        'settings'   => 'banner_cta_text',
    ));
    $wp_customize->add_setting('banner_cta_page', array());
    $wp_customize->add_control('esfbrasil_banner_cta_page', array(
        'label'      => __('Link do botão 1', 'esfbrasil'),
        'section'    => 'esfbrasil_banner_section',
        'type'       => 'dropdown-pages',
        'settings'   => 'banner_cta_page',
    ));
    $wp_customize->add_setting('banner_cta_text2', array(
        'default'        => 'Botão 2',
    ));
    $wp_customize->add_control('esfbrasil_banner_cta_text2', array(
        'label'      => __('Texto do botão 2', 'esfbrasil'),
        'section'    => 'esfbrasil_banner_section',
        'settings'   => 'banner_cta_text2',
    ));
    $wp_customize->add_setting('banner_cta_page2', array());
    $wp_customize->add_control('esfbrasil_banner_cta_page2', array(
        'label'      => __('Link do botão 2', 'esfbrasil'),
        'section'    => 'esfbrasil_banner_section',
        'type'       => 'dropdown-pages',
        'settings'   => 'banner_cta_page2',
    ));

    // Página inicial
    $wp_customize->add_section('esfbrasil_index_section', array(
        'title'       => __('Blocos da página Inicial', 'esfbrasil'),
        'description' =>  'Definição dos blocos de conteúdo da página inicial.',
        'priority'    => 120,
    ));
    for ($n = 1; $n <= 5; $n++) {
        $wp_customize->add_setting('index_section' . $n . '_titulo', array(
            'default'        => '',
        ));
        $wp_customize->add_control('esfbrasil_index_section' . $n . '_titulo', array(
            'label'      => __('Bloco ' . $n . ': Título', 'esfbrasil'),
            'section'    => 'esfbrasil_index_section',
            'settings'   => 'index_section' . $n . '_titulo',
        ));
        $wp_customize->add_setting('index_section' . $n . '_page', array());
        $wp_customize->add_control('esfbrasil_index_section' . $n . '_page', array(
            'label'      => __('Bloco ' . $n . ': Página', 'esfbrasil'),
            'section'    => 'esfbrasil_index_section',
            'type'       => 'dropdown-pages',
            'settings'   => 'index_section' . $n . '_page',
        ));
        $wp_customize->add_setting('index_section' . $n . '_color', array(
            'default'    => 'white'
        ));
        $wp_customize->add_control('esfbrasil_index_section' . $n . '_color', array(
            'settings' => 'index_section' . $n . '_color',
            'label'   => 'Bloco ' . $n . ': Cor de fundo',
            'section'  => 'esfbrasil_index_section',
            'type'    => 'select',
            'choices' => array(
                'white' => 'Branco',
                'dark'  => 'Cinza',
                'green' => 'Verde',
            ),
        ));
    }

    // Rodapé página inicial
    $wp_customize->add_section('esfbrasil_index_footer_section', array(
        'title'       => __('Rodapé da página Inicial', 'esfbrasil'),
        'description' =>  'Configuração do rodapé da página inicial.',
        'priority'    => 121,
    ));
    $wp_customize->add_setting('index_footer_enabled', array(
        'default' => false
    ));
    $wp_customize->add_control('esfbrasil_index_footer_enabled', array(
        'settings' => 'index_footer_enabled',
        'label'    => 'Habilitado',
        'section'  => 'esfbrasil_index_footer_section',
        'type'     => 'checkbox',
    ));
    $wp_customize->add_setting('index_footer_titulo', array(
        'default'        => '',
    ));
    $wp_customize->add_control('esfbrasil_index_footer_titulo', array(
        'label'      => __('Rodapé: Título', 'esfbrasil'),
        'section'    => 'esfbrasil_index_footer_section',
        'settings'   => 'index_footer_titulo',
    ));
    $wp_customize->add_setting('index_footer_page', array());
    $wp_customize->add_control('esfbrasil_index_footer_page', array(
        'label'      => __('Rodapé: Página', 'esfbrasil'),
        'section'    => 'esfbrasil_index_footer_section',
        'type'       => 'dropdown-pages',
        'settings'   => 'index_footer_page',
    ));

    // Redes sociais
    $wp_customize->add_section('esfbrasil_redessociais_section', array(
        'title'    => __('Redes sociais e contato', 'esfbrasil'),
        'description' => 'Links para as redes sociais e email de contato.',
        'priority' => 122,
    ));
    $wp_customize->add_setting('redessociais_facebook', array(
        'default'        => '',
    ));
    $wp_customize->add_control('esfbrasil_redessociais_facebook', array(
        'label'      => __('Link do Facebook', 'esfbrasil'),
        'section'    => 'esfbrasil_redessociais_section',
        'settings'   => 'redessociais_facebook',
    ));
    $wp_customize->add_setting('redessociais_instagram', array(
        'default'        => '',
    ));
    $wp_customize->add_control('esfbrasil_redessociais_instagram', array(
        'label'      => __('Link do Instagram', 'esfbrasil'),
        'section'    => 'esfbrasil_redessociais_section',
        'settings'   => 'redessociais_instagram',
    ));
    $wp_customize->add_setting('redessociais_linkedin', array(
        'default'        => '',
    ));
    $wp_customize->add_control('esfbrasil_redessociais_linkedin', array(
        'label'      => __('Link do LinkedIn', 'esfbrasil'),
        'section'    => 'esfbrasil_redessociais_section',
        'settings'   => 'redessociais_linkedin',
    ));
    $wp_customize->add_setting('redessociais_youtube', array(
        'default'        => '',
    ));
    $wp_customize->add_control('esfbrasil_redessociais_youtube', array(
        'label'      => __('Link do Youtube', 'esfbrasil'),
        'section'    => 'esfbrasil_redessociais_section',
        'settings'   => 'redessociais_youtube',
    ));
    $wp_customize->add_setting('redessociais_email', array(
        'default'        => '',
    ));
    $wp_customize->add_control('esfbrasil_redessociais_email', array(
        'label'      => __('Email de contato', 'esfbrasil'),
        'section'    => 'esfbrasil_redessociais_section',
        'settings'   => 'redessociais_email',
    ));

/*
    //  =============================
    //  = Radio Input               =
    //  =============================
    $wp_customize->add_setting('esfbrasil_theme_options[color_scheme]', array(
        'default'        => 'value2',
        'capability'     => 'edit_theme_options',
        'type'           => 'option',
    ));
 
    $wp_customize->add_control('esfbrasil_color_scheme', array(
        'label'      => __('Color Scheme', 'esfbrasil'),
        'section'    => 'esfbrasil_color_scheme',
        'settings'   => 'esfbrasil_theme_options[color_scheme]',
        'type'       => 'radio',
        'choices'    => array(
            'value1' => 'Choice 1',
            'value2' => 'Choice 2',
            'value3' => 'Choice 3',
        ),
    ));
 
    //  =============================
    //  = Checkbox                  =
    //  =============================
    $wp_customize->add_setting('esfbrasil_theme_options[checkbox_test]', array(
        'capability' => 'edit_theme_options',
        'type'       => 'option',
    ));
 
    $wp_customize->add_control('display_header_text', array(
        'settings' => 'esfbrasil_theme_options[checkbox_test]',
        'label'    => __('Display Header Text'),
        'section'  => 'esfbrasil_color_scheme',
        'type'     => 'checkbox',
    ));
 
 
    //  =============================
    //  = Select Box                =
    //  =============================
     $wp_customize->add_setting('esfbrasil_theme_options[header_select]', array(
        'default'        => 'value2',
        'capability'     => 'edit_theme_options',
        'type'           => 'option',
 
    ));
    $wp_customize->add_control( 'example_select_box', array(
        'settings' => 'esfbrasil_theme_options[header_select]',
        'label'   => 'Select Something:',
        'section' => 'esfbrasil_color_scheme',
        'type'    => 'select',
        'choices'    => array(
            'value1' => 'Choice 1',
            'value2' => 'Choice 2',
            'value3' => 'Choice 3',
        ),
    ));
 
 
    //  =============================
    //  = Image Upload              =
    //  =============================
    $wp_customize->add_setting('esfbrasil_theme_options[image_upload_test]', array(
        'default'           => 'image.jpg',
        'capability'        => 'edit_theme_options',
        'type'           => 'option',
 
    ));
 
    $wp_customize->add_control( new WP_Customize_Image_Control($wp_customize, 'image_upload_test', array(
        'label'    => __('Image Upload Test', 'esfbrasil'),
        'section'  => 'esfbrasil_color_scheme',
        'settings' => 'esfbrasil_theme_options[image_upload_test]',
    )));
 
    //  =============================
    //  = File Upload               =
    //  =============================
    $wp_customize->add_setting('esfbrasil_theme_options[upload_test]', array(
        'default'           => 'arse',
        'capability'        => 'edit_theme_options',
        'type'           => 'option',
 
    ));
 
    $wp_customize->add_control( new WP_Customize_Upload_Control($wp_customize, 'upload_test', array(
        'label'    => __('Upload Test', 'esfbrasil'),
        'section'  => 'esfbrasil_color_scheme',
        'settings' => 'esfbrasil_theme_options[upload_test]',
    )));
 
 
    //  =============================
    //  = Color Picker              =
    //  =============================
    $wp_customize->add_setting('esfbrasil_theme_options[link_color]', array(
        'default'           => '#000',
        'sanitize_callback' => 'sanitize_hex_color',
        'capability'        => 'edit_theme_options',
        'type'           => 'option',
 
    ));
 
    $wp_customize->add_control( new WP_Customize_Color_Control($wp_customize, 'link_color', array(
        'label'    => __('Link Color', 'esfbrasil'),
        'section'  => 'esfbrasil_color_scheme',
        'settings' => 'esfbrasil_theme_options[link_color]',
    )));
 
 
    //  =============================
    //  = Page Dropdown             =
    //  =============================
    $wp_customize->add_setting('esfbrasil_theme_options[page_test]', array(
        'capability'     => 'edit_theme_options',
        'type'           => 'option',
 
    ));
 
    $wp_customize->add_control('esfbrasil_page_test', array(
        'label'      => __('Page Test', 'esfbrasil'),
        'section'    => 'esfbrasil_color_scheme',
        'type'    => 'dropdown-pages',
        'settings'   => 'esfbrasil_theme_options[page_test]',
    ));

    // =====================
    //  = Category Dropdown =
    //  =====================
    $categories = get_categories();
	$cats = array();
	$i = 0;
	foreach($categories as $category){
		if($i==0){
			$default = $category->slug;
			$i++;
		}
		$cats[$category->slug] = $category->name;
	}
 
	$wp_customize->add_setting('_s_f_slide_cat', array(
		'default'        => $default
	));
	$wp_customize->add_control( 'cat_select_box', array(
		'settings' => '_s_f_slide_cat',
		'label'   => 'Select Category:',
		'section'  => '_s_f_home_slider',
		'type'    => 'select',
		'choices' => $cats,
	));
*/
}
add_action('customize_register', 'esfbrasil_customize_register');

require (get_template_directory() . '/shortcode-mapa.php');
add_shortcode( 'mapa_nucleos', 'mapa_shortcode' );


function my_login_logo() {
    echo '<style type="text/css">body.login div#login h1 a {background-image: url('.get_template_directory_uri().'/assets/img/icon.png); padding-bottom: 30px;}</style>';
}
add_action( 'login_enqueue_scripts', 'my_login_logo' );
?>
